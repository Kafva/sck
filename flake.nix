{
    # nix flake --help
    # https://nixos.wiki/wiki/Flakes#Flake_schema
    description = "sck - suckless tools";

    inputs = {
        nixpkgs.url = "github:nixos/nixpkgs/nixpkgs-unstable";
    };

    outputs = {self, nixpkgs}:
    let
        name = "sck";
        system = "x86_64-linux";
        pkgs = import nixpkgs { inherit system; };
        nativeBuildInputs = with pkgs; [
            makeWrapper
            xorg.libX11
            xorg.libXext
            xorg.libXft
            xorg.libXrender
            xorg.libXinerama
            xorg.libXrandr
            pkg-config
            fontconfig
            freetype
            harfbuzz
            imagemagick
        ];
    in
    {
         # Ran with: `nix develop`
         devShells."${system}".default = pkgs.mkShell {
            packages = nativeBuildInputs;
         };

         packages."${system}" = rec {
            # Ran with: `nix build`
            default = pkgs.stdenv.mkDerivation {
                name = name;
                src = self;

                # Compile time dependencies
                nativeBuildInputs = nativeBuildInputs;
                buildPhase = ''
                   make
                '';
                installPhase = ''
                   make install PREFIX= DESTDIR=$out
                '';
            };
            # Alias for the default target, we can also specify `default`
            # directly.
            #
            #   environment.systemPackages = [
            #        inputs.sck.packages."${system}"."${name}"
            #   ];
            #
            #
            "${name}" = default;
         };
    };
}
