# sck
Suckless software forks.

To develop with nix
```bash
nix develop --command $SHELL
```

## st
* Scroll: <kbd>Super</kbd> <kbd>Shift</kbd> <kbd>Up</kbd> / <kbd>Super</kbd> <kbd>Shift</kbd> <kbd>Down</kbd>
* Zoom: <kbd>Super</kbd> <kbd>+</kbd> / <kbd>Super</kbd> <kbd>-</kbd>
* Reset zoom: <kbd>Super</kbd> <kbd>0</kbd>
* Copy cursor selection: <kbd>Super</kbd> <kbd>c</kbd> (unused in favor of `tmux`)
* Paste X clipboard buffer: <kbd>Super</kbd> <kbd>v</kbd>
