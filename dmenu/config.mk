# dmenu version
VERSION = 5.0

# paths
PREFIX = /usr/local
MANPREFIX = $(PREFIX)/share/man

UNAME := $(shell uname)

ifeq ($(UNAME), Linux)
	_PREFIX=/usr
	X11INC = /usr/X11R6/include
	X11LIB = /usr/X11R6/lib
endif
ifeq ($(UNAME), FreeBSD)
	_PREFIX=/usr/local
	X11INC = ${_PREFIX}/include
	X11LIB = ${_PREFIX}/lib
endif

XINERAMALIBS  = -lXinerama
XINERAMAFLAGS = -DXINERAMA

FREETYPELIBS = -lfontconfig -lXft
FREETYPEINC = ${_PREFIX}/include/freetype2

# includes and libs
INCS = -I$(X11INC) -I$(FREETYPEINC)
LIBS = -L$(X11LIB) -lX11 $(XINERAMALIBS) $(FREETYPELIBS)

# flags
CPPFLAGS = -D_DEFAULT_SOURCE -D_BSD_SOURCE -D_XOPEN_SOURCE=700 -D_POSIX_C_SOURCE=200809L -DVERSION=\"$(VERSION)\" $(XINERAMAFLAGS)
CFLAGS   = -std=c99 -pedantic -Wall -Os $(INCS) $(CPPFLAGS) -Wno-int-in-bool-context
LDFLAGS  = $(LIBS)

# compiler and linker
CC = cc
