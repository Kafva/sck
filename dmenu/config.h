/* See LICENSE file for copyright and license details. */
/* Default settings; can be overriden by command line. */
static int topbar = 1; /* -b  option; if 0, dmenu appears at bottom     */

static const char *fonts[] = { "Montserrat:size=12:style=bold" };

/* -p  option; prompt to the left of input field */
static const char *prompt = NULL;
static const char *colors[SchemeLast][2] = {
  /*               fg         bg        */
  [SchemeNorm] = { "#f5e4f3",   "#1e2320" },
  [SchemeSel] =  { "#6e85d3",   "#1e2320" },
  [SchemeOut] =  { "#000000",   "#1e2320" },
};
/* -l option; if nonzero, dmenu uses vertical list with given number of lines */
static unsigned int lines      = 0;

/*
 * Characters not considered part of a word while deleting words
 * for example: " /?\"&[]"
 */
static const char worddelimiters[] = " ";
