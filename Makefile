.PHONY: clean install

DESTDIR ?= /usr
MAKE ?= make

all: dmenu/dmenu st/st

dmenu/dmenu:
	$(MAKE) -C dmenu

st/st: st/icon.h
	$(MAKE) -C st st

# Generate a NETWM icon into st/icon.h
# https://raw.githubusercontent.com/xdavidel/st/master/netwmicon.sh
st/icon.h:
	echo 'unsigned long icon[] = {' >> $@
	identify -format '%w, %h,\n' "st/st.png" | sed 's/^/    /' >> $@
	convert -background none "st/st.png" RGBA: | \
		hexdump -ve '"0x%08x, "' | fmt | sed 's/^/    /' >> $@
	echo "};" >> $@

clean:
	rm -f st/icon.h
	$(MAKE) -C dmenu clean
	$(MAKE) -C st clean

install: dmenu/dmenu st/st
	$(MAKE) -C dmenu install
	$(MAKE) -C st install

